<?php

namespace Pantagruel74\Yii2Seeder;

use Pantagruel74\Yii2Seeder\helpers\ClassesSearchHelper;
use yii\base\Model;

class SeederManager extends Model
{
    public string $seedersPath;
    public bool $verbal = false;

    /**
     * @return string[]
     */
    protected function searchAllClasses(): array
    {
        return ClassesSearchHelper::getClasses($this->seedersPath);
    }

    /**
     * @return void
     */
    public function all(): string
    {
        $result = [];
        foreach ($this->searchAllClasses() as $seederClassName)
        {
            $seeder = new $seederClassName([
                'verbal' => $this->verbal,
            ]);
            $n = $seeder->run();
            $result[] =  $seederClassName . " seed " . $n . " records";
            unset($seeder);
        }
        return implode("\n", $result);
    }

    /**
     * @return string
     */
    public function first(): string
    {
        $result = [];
        $seederClassName = current($this->searchAllClasses());
        $seeder = new $seederClassName([
            'verbal' => $this->verbal,
        ]);
        $n = $seeder->run();
        return $seederClassName . " seed " . $n . " records";
    }

    /**
     * @return void
     */
    public function view(): void
    {
        echo implode("\n", $this->searchAllClasses());
    }
}