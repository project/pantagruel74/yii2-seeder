<?php

namespace Pantagruel74\Yii2Seeder\helpers;

use Pantagruel74\Yii2Seeder\Seeder;
use function WyriHaximus\listInstantiatableClassesInDirectories;

final class ClassesSearchHelper
{
    public static function getClasses(string $folderPath): array
    {
        $classFiles = iterator_to_array(listInstantiatableClassesInDirectories($folderPath));
        $classFiles = array_filter($classFiles, fn(string $c) => (is_subclass_of($c, Seeder::class)));
        return array_values($classFiles);
    }
}