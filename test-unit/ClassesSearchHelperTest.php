<?php

namespace Pantagruel74\Yii2SeederTestUnit;

use Pantagruel74\Yii2Seeder\helpers\ClassesSearchHelper;
use PHPUnit\Framework\TestCase;

class ClassesSearchHelperTest extends TestCase
{
    public function testSearch1(): void
    {
        $classes = ClassesSearchHelper::getClasses(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'test-unit');
        $this->assertEquals(['Pantagruel74\Yii2SeederTestUnit\ttt\TestSeeder'], $classes);
    }

    public function testSearch2(): void
    {
        $classes = ClassesSearchHelper::getClasses(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'stubs');
        $this->assertEquals([
            'Pantagruel74\Yii2SeederStub\SeederStub1',
            'Pantagruel74\Yii2SeederStub\SeederStub2',
        ], $classes);
    }
}