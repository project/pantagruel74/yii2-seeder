<?php

namespace Pantagruel74\Yii2SeederTestUnit;

use Pantagruel74\Yii2Loader\Yii2Loader;
use Pantagruel74\Yii2Seeder\SeederManager;
use PHPUnit\Framework\TestCase;

class SeederManagerTest extends TestCase
{
    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        Yii2Loader::load();
        parent::__construct($name, $data, $dataName);
    }

    public function testSeedAll(): void
    {
        $ds = DIRECTORY_SEPARATOR;
        $manager = new SeederManager([
            'seedersPath' => dirname(__DIR__) . $ds . 'stubs',
        ]);
        $result = $manager->all();
        $this->assertEquals("Pantagruel74\Yii2SeederStub\SeederStub1 seed 12 records\n"
            . "Pantagruel74\Yii2SeederStub\SeederStub2 seed 42 records", $result);
    }
}